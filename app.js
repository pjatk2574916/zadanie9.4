const express = require('express');
const redis = require('redis');
const { Pool } = require('pg');

const app = express();
app.use(express.json());

const redisClient = redis.createClient({
  url: 'redis://redis:6379'
});

redisClient.connect().catch(console.error);

const pool = new Pool({
  user: 'dbuser',
  host: 'postgres',
  database: 'mydb',
  password: 'secretpassword',
  port: 5432,
});

app.post('/messages', async (req, res) => {
  const { message } = req.body;
  try {
    await redisClient.rPush('messages', message);
    res.send(`Message saved: ${message}`);
  } catch (err) {
    res.status(500).send('Error saving message to Redis');
  }
});

app.get('/messages', async (req, res) => {
  try {
    const messages = await redisClient.lRange('messages', 0, -1);
    res.json(messages);
  } catch (err) {
    res.status(500).send('Error retrieving messages from Redis');
  }
});

app.post('/users', async (req, res) => {
  const { name, email } = req.body;
  try {
    const result = await pool.query('INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *', [name, email]);
    res.json(result.rows[0]);
  } catch (err) {
    res.status(500).send('Error saving user to PostgreSQL');
  }
});

app.get('/users', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM users');
    res.json(result.rows);
  } catch (err) {
    res.status(500).send('Error retrieving users from PostgreSQL');
  }
});

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
